## Servidor de llicencies nvidia vgpu

Create a desktop based on debian10 template OS downloaded from updates and from debian10.iso latest:

![](./license_server_img.ca/1.png)
![](./license_server_img.ca/2.png)
![](./license_server_img.ca/3.png)

Graphical install with default options.

Enter hostname:

![](./license_server_img.ca/4.png)

user: root / pwd: pirineus

user: isard / pwd: pirineus

![](./license_server_img.ca/5.png)

![](./license_server_img.ca/6.png)

![](./license_server_img.ca/7.png)

Installing the base system...

Instalar la versión libre de java y sudo que lo necesita el binario instalador de nvidia

```
apt install -y default-jre
```

### Tomcat siguiendo instrucciones de nvidia

Seguimos las instrucciones que da nvidia para instalar el software en su documentación: [https://docs.nvidia.com/grid/ls/latest/grid-license-server-user-guide/index.html#registering-license-server-and-getting-license-files](https://docs.nvidia.com/grid/ls/latest/grid-license-server-user-guide/index.html#registering-license-server-and-getting-license-files)

```
# añadir grupo y usuario tomcat y hacer instalación "manual" de tomcat, no usar la 
# tomcat que viene con la distro 
groupadd tomcat
useradd -d /usr/share/tomcat -g tomcat -M -s /bin/false tomcat
mkdir -p /usr/share/tomcat

wget https://downloads.apache.org/tomcat/tomcat-9/v9.0.68/bin/apache-tomcat-9.0.68.tar.gz
tar xvf apache-tomcat-*.tar.gz -C /usr/share/tomcat --strip-components 1
cd /usr/share/tomcat/
chgrp -R tomcat /usr/share/tomcat
chmod -R g+r conf
chmod g+x conf
chown -R tomcat webapps work temp logs

#fichero de servicio

cat > /etc/systemd/system/tomcat.service << "EOF"
[Unit]
Description=Apache Tomcat Server
After=network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/lib/jvm/default-java
Environment=CATALINA_PID=/usr/share/tomcat/temp/tomcat.pid
Environment=CATALINA_HOME=/usr/share/tomcat
Environment=CATALINA_BASE=/usr/share/tomcat
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/usr/share/tomcat/bin/startup.sh
ExecStop=/usr/share/tomcat/bin/shutdown.sh

User=tomcat
Group=tomcat
UMask=0007
RestartSec=15
Restart=always

[Install]
WantedBy=multi-user.target
EOF


systemctl daemon-reload
systemctl enable tomcat.service
systemctl start tomcat.service

```

### instalación en consola interactiva

En nvidia en la zona de downloads restringida a los usurios registrados encontramos el fichero: **NVIDIA-ls-linux-2022.09-2022.09.0.31771813.zip**

https://ui.licensing.nvidia.com/software

![](./license_server_img.ca/8.png)

Dentro encontramos un setup.bin que copiamos en /root del servidor

```
apt install unzip sudo -y 
unzip NVIDIA-ls-linux-2022.09-2022.09.0.31771813.zip -d /root/
cd /root
chmod u+x setup.bin
./setup.bin -i console -r responses.txt
```

Durante la instlación dos detalles imporantes:

![](./license_server_img.ca/9.png)

1. Cuando avisa del path donde tenemos el tomcat, hay que copiar y pegar el path que no sugiere:

```
TOMCAT_SERVER_PATH=/usr/share/tomcat
```

2. Cuando nos pide la configuración del firewall hemos de decirle que habra los dos puertos para poder entrar al tomcat vía web, si no habríamos de instalar unas X para poder configurar el servidor. Una vez configurado ya lo segurizaremos con iptables manualmente nosotros. 
   Hay que poner **1,2** para permitir conexiones desde cualquier ip, no sólo localhost

```
 The license server's management interface listens on port 8080. Leave this 
port closed to prevent unauthorized access to the management interface.

->1- License server (port 7070)
  2- Management interface (port 8080)

ENTER A COMMA-SEPARATED LIST OF NUMBERS REPRESENTING THE DESIRED CHOICES, OR
   PRESS <ENTER> TO ACCEPT THE DEFAULT: 1,2

```

Una vez finalizada la instlaación podemos acceder vía web al servidor de licencias a través de http://direcciónip:8080/licserver

![](./license_server_img.ca/10.png)

Antes de nada, hay que introducir correctamente la MAC address que queramos usar para crear el servidor de licencias, con esto se asocia la MAC con las licencias, si esta es diferente saltara un error que no deja introducir el fichero de licencia y habria que cambiar la MAC de la interfaz utilizada.

En este caso se añade una nueva interfaz a la maquina que se usa de DHCP con esta configuración:

```
# dhcpd.conf
#
# Sample configuration file for ISC dhcpd
#

# option definitions common to all supported networks...
#option domain-name "example.org";
#option domain-name-servers ns1.example.org, ns2.example.org;

default-lease-time 600;
max-lease-time 7200;

# The ddns-updates-style parameter controls whether or not the server will
# attempt to do a DNS update when a lease is confirmed. We default to the
# behavior of the version 2 packages ('none', since DHCP v2 didn't
# have support for DDNS.)
ddns-update-style none;

# If this DHCP server is the official DHCP server for the local
# network, the authoritative directive should be uncommented.
#authoritative;

# Use this to send dhcp log messages to a different log file (you also
# have to hack syslog.conf to complete the redirection).
#log-facility local7;

# No service will be given on this subnet, but declaring it helps the 
# DHCP server to understand the network topology.

subnet 192.168.120.0 netmask 255.255.252.0 {
}


# This is a very basic subnet declaration.

subnet 172.17.120.0 netmask 255.255.252.0 {
  range 172.17.120.10 172.17.123.200;
}
```
Una vez comprobado que el servidor DHCP da las IPs a los clientes correctamente, ir a la web de nvidia: https://nvid.nvidia.com

una vez validados ir al NVIDIA LICENSING PORTAL:

![](./license_server_img.ca/11.png)

Y crear un servidor de licencias:

![](./license_server_img.ca/12.png)

![](./license_server_img.ca/13.png)

![](./license_server_img.ca/14.png)

![](./license_server_img.ca/15.png)

![](./license_server_img.ca/16.png)

Y se crea el entitlement donde se ve la cantidad de licencias que hemos asignado a ese servidor:

![](./license_server_img.ca/17.png)

![](./license_server_img.ca/18.png)

Y en la parte de servidor, desplegamos y descargamos el fichero de la licencia:

![](./license_server_img.ca/19.png)

Una vez descargado, desde el navegador conectados a nuestro servidor de licencias:

![](./license_server_img.ca/20.png)

Subir el fichero desde License Management:

![](./license_server_img.ca/21.png)

![](./license_server_img.ca/22.png)

Y ha de aparecer un mensaje:

![](./license_server_img.ca/23.png)

 Si no coinciden las direcciones MAC da error al hacer upload.

Y aparecen las licencias:

![](./license_server_img.ca/24.png)

Hay que revisar si tiene las 48 licencias anteriormente introducidas, si no volver a descargar el archivo y hacer el upload.

Ahora lo mejor es fijar la ip de este servidor:

Instalamos network-manager, quitamos la interface de /etc/network/interfaces y configuramos con nmtui

```
apt install network-manager
nmtui
```

Lo dejamos fijo en 192.168.122.115

