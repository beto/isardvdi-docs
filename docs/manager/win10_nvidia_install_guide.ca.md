## Instal·lar drivers de nvidia i accedir per RDP a un desktop de windows 10

Crear escriptori des de template:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616151858.png)



### Opcions de l'escriptori per l'instal·lació

Editar l'escriptori:

* canviar el Vídeo de **default** a **NVIDIA with QXL** 
* afegir una segona interface seleccionant amb la tecla **ctrl** apretada la xarxa **Wireguard VPN**
* afegir el cd **drivers_nvidia_461_33.iso**

![](./win10_nvidia_install_guide_img.ca/screenshot_1616153389.png)



### Instal·lar el driver

Ens connectem a l'escriptori amb un visor a l'escriptori i obrim el CD. Executem l'instal·lador de nvidia:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616154985.png)



I seguim les passes que ens proposa:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616155012.png)

![](./win10_nvidia_install_guide_img.ca/screenshot_1616155052.png)

![](./win10_nvidia_install_guide_img.ca/screenshot_1616155071.png)

![](./win10_nvidia_install_guide_img.ca/screenshot_1616155091.png)

I li diem que volem reiniciar més tard quan acava la instal·lació:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616155143.png)

### Activar connexions remotes per RDP al windows

escrivim "remote" a la barra de cerca del windows i seleccionem **"Configuración de Escritorio remoto"**

![](./win10_nvidia_install_guide_img.ca/screenshot_1616156041.png)

Activem l'opció i confirmem:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616156101.png)

Ara clic a **Configuración avanzada**:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616156139.png)

I desactivem l'opció **Requereir equipos uen autenticación a nivel de red para conectarse** i validem l'opció amb clic al botó **Continua de totes maneres**:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616156181.png)

![](./win10_nvidia_install_guide_img.ca/screenshot_1616156249.png)

Ja podem tancar aquest menú i tenim l'accés per RDP activat per usuaris locals.



Ara fem una apagada ordenada:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616155243.png)



### Editar el desktop per accedir per RDP

Una vegada els drivers estan instal·lats ja no podem fer servir les dues targetes GPU, cal canviar per tal que solament pugui fer servir Nvidia amb RDP. Editem el desktop:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616155624.png)

Ara fem start del desktop i esperem a que aparegui la IP per poder accedir per RDP:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616155680.png)

Per exemple en aquest cas tenim la ip 10.2.0.187 i si tenim la VPN d'Isard activa ja es pot entrar amb un client d'escriptori remot de windows. Es pot fer servir el client que porten per defecte els windows, o per exemple en linux es pot fer servir el remmina.



## Accedir per RDP

Farem servir un usuari avançat (professor). Ens validem a Isard i accedim a aquesta pantalla 

![](./win10_nvidia_install_guide_img.ca/screenshot_1616240906.png)

De moment l'accés per VPN-RDP encara no està disponible a la interfície simplificada. Cal anar a la virusalització d'**administració**. 

![](./win10_nvidia_install_guide_img.ca/screenshot_1616241077.png)



Creem un escriptori nou:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616241043.png)

Ara fem start a l'escriptori i esperem que agafi una ip, depenen del sistema operatiu triga més o menys a agafar la IP. Primera pantalla sense IP:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616241148.png)

Per exemple amb aquesta plantilla ha trigat uns 30 segons a donar una ip. Pantalla amb IP:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616241254.png)

Aquesta adreça IP **10.3.2.190** ha de ser accesible pel nostre ordinador client mitjançant una connexió VPN (xarxa privada virtual) que establirà una connexió xifrada entre el nostre ordinador i el servidor d'isard, i que ens permetrà accedir als nostres escriptoris. Aquesta connexió VPN ens servirà per accedir a tots els escriptoris que siguin nostres i cal configurar-la per cada usuari. 

És a dir, si un alumne o professor es connecta des del seu ordinador personal de casa ha de fer el procediment de crear la VPN una vegada. Si ho fa des d'un ordinador del centre ho haurà de fer amb la seva sessió.

Si no hem configurat la vpn podem tancar aquest menú i seguir les indicacions següents.

### Instal·lar VPN

A la part inferior del llistat de desktops tenim un enllaç que indica com instal·lar un client de wireguard. Wireguard és el tipus de VPN que hem servir.

![](./win10_nvidia_install_guide_img.ca/screenshot_1616241573.png)

Primer hem d'accedir a l'enllaç **"Install wireguard client"** 

Farem l'exemple amb windows:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616241655.png)

Descarreguem l'instal·lador i arribem a aquesta pantalla:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616241768.png)

Tornem a l'Isard i descarraguem el fitxer de configuració amb el botó **Download vpn config file**:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616241813.png)

Guardem el fitxer i el carreguem amb el botó **Import tunnel(s) from file**:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616241899.png)

I al wireguard ens apareix la configuració llesta per activar:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616241951.png)

Fem clic a Activate i canvia el **Status a Active**:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616242015.png)

Ara ja hauríem de tenir contacte amb el nostre escriptori mitjançant aquesta VPN. Ho podem provar fent un ping. Amb la combinació **tecla-windows + R** apareix el menú per executar una comanda i escrivim **cmd**:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616242092.png)

Fem un ping a la ip de l'escriptori que ens informa a la part dels visors:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616242155.png)

I hauríem d'obtenir resposta:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616242205.png)



Ara ens podem descarregar el fitxer de configuració de la connexió RDP fent clic al botó **"RDP Application"** i li podem dir que el volem obrir amb "**Conexión a Escritorio remoto**":

![](./win10_nvidia_install_guide_img.ca/screenshot_1616243802.png)

Surt una alerta de seguretat, li responem que volem connectar-nos:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616243889.png)

i ens demana la contrasenya d'admin, en aquest cas: pirineus

I ja tenim accés a l'escriptori, en el client de windows de RDP apareix una barra superior que ens permet canviar de mode pantalla completa, apagar el visor...

![](./win10_nvidia_install_guide_img.ca/screenshot_1616242845.png)

Ja podem fer servir programes amb necessitat de 3D:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616243062.png)



### Connexió manual a l'escriptori remot

Podem connectar-nos pel client d'escriptori remot de windows manualment a partir de la IP. Busquem per **remote** a la barra de tasques del windows i accedim a **"Conexión a Escritorio remoto"**

![](./win10_nvidia_install_guide_img.ca/screenshot_1616242417.png)

L'aplicació ens demana pel nom d'equip, podem introduir la IP de l'escriptori d'Isard:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616242517.png)

I ara ens demana les credencials per accedir a l'escriptori. Aquestes credencials són les de l'usuari i password de windows que té l'escriptori, per defecte a les plantilles d'Isard son:

* usuari: admin
* pwd: pirineus

![](./win10_nvidia_install_guide_img.ca/screenshot_1616242747.png)

Ens demana si volem confiar en el certificat, li diem que confiem i que no cal que ens torni a preguntar:

![](./win10_nvidia_install_guide_img.ca/screenshot_1616242788.png)

