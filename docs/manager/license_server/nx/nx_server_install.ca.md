# Instal·lació del servidor de llicències de NX, SPLM License Server

## Requisits previs

Abans de fer la instal·lació, el proveïdor ens ha de donar una utilitat per generar el 'composite'. Bàsicament és un fitxer generat a partir del nom de l'equip on ha d'anar el servidor, la seva MAC i alguna dada més. Aquest fitxer s'envia al proveïdor per generar la llicència.

![](nx_server_install.ca/img1.png)

![](nx_server_install.ca/img3.png)

## Instal·lació del servidor

Executarem **SPLMLicenseServer**. la primera cosa que ens demanarà serà l'idioma

![](nx_server_install.ca/img5.png)

S'inicia la instal·lació

![](nx_server_install.ca/img6.png)

Triem el destí i l'emplaçament del fitxer de llicència

![](nx_server_install.ca/img7.png)

![](nx_server_install.ca/img8.png)

S'ha de confirmar abans de començar

![](nx_server_install.ca/img9.png)

![](nx_server_install.ca/img10.png)

![](nx_server_install.ca/img11.png)

## Posada en marxa.

Si tot ha anat be, tindrem un nou servei en execució: **Siemens PLM License Service**. Podem verificar l'estat del servidor amb la utilitat '**lmtools**'. Si ho escrivim al control de cerca de la barra d'estat de Windows...

![](nx_server_install.ca/img13.png)

![](nx_server_install.ca/img14.png)

A '**Server Status**' podem fer clic al botó '**Perform Status Enquiry**' per veure el nombre de llicències habilitades

![](nx_server_install.ca/img15.png)

## Canvi o renovació de llicència.

Iniciarem '**lmtools**' segons s'explica a 'Posada en Marxa' i anirem a '**Config Services**'. A '**Path to the license File**' busquem el fitxer de llicencies nou amb el botó '**Browse**' i fem clic a '**Save Service**'

![](nx_server_install.ca/img16.png)

![](nx_server_install.ca/img17.png)

Seguidament anem a '**Start/Stop/Reread**' per para el servei i tornar-lo a iniciar amb les noves dades. Pot ser necessari activar la casella '**Force Server Shutdown**'.
