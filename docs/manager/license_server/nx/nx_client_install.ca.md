# Instal·lació del client de NX

## Requisits previs.

IP del Servidor de LLicències de NX previament instal·lat, el programa de instal·lació ens la demanarà durant el procés.

La configuració de la xarxa he de permetre la visibilitat entre client y servidor.

## Instal·lació del client NX.

Dins de la carpeta del programa de instal·lació hi ha d'haver l'executable 'Launch.exe'. Fent doble clic s'obre la finestra on podem triar entre diferents opcions. En aquest cas en centrem en l'opció 'INSTALL NX'

![](nx_client_install.ca/img1.png)

Iniciar fent clic a sobre de INSTALL NX. Ens demanarà de triar idioma per fer la instal·lació.

![](nx_client_install.ca/img2.png)

Seguidament  triem les parts del programa que volem instal·lar.

![](nx_client_install.ca/img4.png)

Després de fer clic a 'Siguiente' se'ns demana per l'adreça del servidor en format port@server-ip. El port acostuma a ser el 28000. Al fer clic a 'Siguiente', l'instal·lador verifica la connexió amb el servidor. Cas de fallar, el programa de instal·lació no podrà continuar.

![](nx_client_install.ca/img5.png)

Un cop feta la verificació, ens torna a demanar per l'idioma, en aquest cas el que farem servir al programa

![](nx_client_install.ca/img6.png)

Després de confirmar la instal·lació, aquesta començarà.

![](nx_client_install.ca/img7.png)

![](nx_client_install.ca/img8.png)

Finalitza amb

![](nx_client_install.ca/img9.png)

## Activació de llicència

Amb el programa ja instal·lat hem de iniciar la 'Herramienta de licenciamiento' abans de poder fer-lo servir. Per això, escriurem 'licenciamiento' al control de cerca de la barra de tasques de Windows, al costat de la icona de Windows. Al desplegable apareix la eina, que iniciem amb un clic.

![](nx_client_install.ca/img10.png)

Apareix la aplicació. Al panel esquerra hi ha diferents opcions.

![](nx_client_install.ca/img11.png)

Des de 'Environment Settings' podrem modificar la connexió amb el servidor.

![](nx_client_install.ca/img12.png)

Per activar la llicència hem d'anar a 'Bundle Settings'. A 'Available Bundles' han d'aparèixer els mòduls pels que disposem de llicència.

![](nx_client_install.ca/img13.png)

S'han de seleccionar i, fent clic a la fletxa, passar-los a 'Applied Bunles'.

![](nx_client_install.ca/img14.png)

Un cop fet, apliquem i sortim. Ja podem iniciar NX, per fer-ho ho escrivim al control de cerca i fent clic s'obrirà l'aplicació

![](nx_client_install.ca/img15.png)

![](nx_client_install.ca/img16.png)

## Canvi de llicència al servidor

En determinades circumstàncies pot ser necessari el canvi o  renovació de la llicència al servidor. Quan es fa aquets canvi s'ha de tornar a repetir el procés de llicenciament descrit anteriorment.
