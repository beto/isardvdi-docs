# System administration

## Firewalld with docker in IsardVDI

If we come from a previous version with firewalld handling the NAT we had:

* firewalld service
  * docker0 interface in public zone
  * masquerade activated (NAT)
* docker service
  * docker with disabled iptables (/etc/docker/daemon.json)

Now, with the actual src it will continue working but the configuration should be modified as follows:

* firewalld service
    * ```firewall-cmd --zone=public --remove-interface docker0 --permanent```
    * ```firewall-cmd --remove-masquerade --permanent```
* docker service
    * Remove /etc/docker/daemon.json or modify it to activate iptables: ```{ "iptables": true }```

Then we should restart firewalld service: ```systemctl restart firewalld``` and restart docker: ```systemctl restart docker```

With this new configuration we will have real client IP inside isard-portal and docker will handle the open/close ports.

In **cloud infrastructure** configurations (docker-compose.hypervisor.yml) the remote hypervisors should open port 2022 at docker level in isard-hypervisor container (not automated yet, you need to edit the config manually) or forward that port inside the isard-hypervisor container at firewalld level:

* ```firewall-cmd --add-forward-port=port=2022:proto=tcp:toport=22:toaddr=172.18.255.17```

NOTE1: 172.18.255 is the envvar DOCKER_NET at isardvdi.conf. Modify accordingly if you modified DOCKER_NET.
 
Also, if you are in **local infrastructure** configurations (docker-compose.hypervisor-standalone.yml) you will need to forward port 2022 and also the full video ports (as this configuration will not have the isard-video included):

* ```firewall-cmd --add-forward-port=port=2022:proto=tcp:toport=22:toaddr=172.18.255.17```
* ```firewall-cmd --add-forward-port=port=5900-6899:proto=tcp:toport=5900-6899:toaddr=172.18.255.17```
