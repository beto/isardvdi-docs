# Upgrade

## From a Quickstart

Just upgrade

```bash
wget https://isardvdi.com/docker-compose.yml
docker-compose pull
docker-compose up -d
```

## From Source/Build

The procedure to upgrade is as follows:

1. Go to your src folder
2. Get new src: ```git pull```
3. Check if there are any new environment vars in isardvdi.cfg.example that you don't have in your current isardvdi.cfg
4. Build yml: ```bash build.sh```
5. Now you can choose between:
  - build your own images: ```docker-compose -f docker-compose.build.yml build```
  - or pull current images: ```docker-compose pull```
6. Bring the new containers up (upgrades will be automated): ```docker-compose up -d```
