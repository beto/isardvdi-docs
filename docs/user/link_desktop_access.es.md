# Acceso al escritorio mediante un enlace

Para generar el enlace, deberemos acceder a la opción "desktops".

![](./link_desktop_access.en.images/captura1.png)

Escogeremos el "desktop" del cual queremos generar el enlace y pulsaremos sobre el siguiente icono:

![](./link_desktop_access.en.images/captura2.png)

En el desplegable que aparece a continuación pulsaremos sobre el icono  "viewer":

![](./link_desktop_access.en.images/captura3.png)

![](./link_desktop_access.en.images/captura4.png)

En la nueva ventana que se abrirá nos aparecerá el enlace para poder acceder al "desktop".

![](./link_desktop_access.en.images/captura5.png)

Para copiarlo pulsaremos sobre el icono  "copy to clipboard" ya que la opción de copiar el texto aparece deshabilitada.

![](./link_desktop_access.en.images/captura6.png)

Una vez copiado, abrimos una ventana en el navegador, pegamos la URL y nos abrirá la siguiente ventana:

![](./link_desktop_access.en.images/captura7.png)

Este enlace se puede enviar a cualquier usuario para poder acceder al escritorio sin tener que repetir este proceso ja que el enlace generado no caduca. 

