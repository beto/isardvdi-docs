# Web Interfaces

There are tree interfaces that gives different access types to IsardVDI, the [direct link](#direct-link), the [*full administration interface*](#full-administration-web-interface) and a [*simplified user interface*](#simplified-user-web-interface). This allows [advanced users](../advanced/index.md) and [manager users](../manager/index.md) to administer templates and company limits and quotas while leaving a simplified interface to the users.

## Direct link

And [advanced](../advanced/index.md) user could provide you a direct link to a virtual desktop. Opening the link with a browser you will can select to access via the browser or 
with a [local client (follow the link for installation instructions)](local-client.md).

## Simplified user web interface

This simplified interface is the main interface when user access to the domain URL root, Eg. <https://desktops.isardvdi.com>. It is intended to bring a simplified user interface to access non-persistent desktops from its user allowed templates. We will extend it to allow manage persistent desktops in the future.

![nonpersistent_login](../images/first-steps/nonpersistent_login.png)

This interface allows:

- Access with IsardVDI users with a username and password.
- Access using accounts from Google and Github. First time you will need a token provided by a [manager](../manager/index.md). Maybe your system administrator won't allow this kind of access.
- Access to live nonpersistent desktops, destroyed on session logout.
- Multi template option

![nonpersistent_login](../images/first-steps/nonpersistent_access.png)

With this simple interface the user is given two kinds of viewers:

- Local client: Using the spice client. Installation instructions are in [Local Client](local-client.md)
- Browser: Using the same browser with HTML5

If the user has access to more than one non-persistent desktop (he has access to more than one template) the system will ask which non persistent desktop wants to create and access on login:

![nonpersistent_login](../images/first-steps/enrollment_templates.png)

## Full administration web interface

This interface is intended for [Administrator](../admin/index.md), [Manager](../manager/index.md) and [Advanced](../advanced/index.md) roles as it brings them full control. Normal [users](index.md) can still access this admin interface if they are allowed to have persistent desktops.

The URL for accessing to this web interface for *Default* category is in **/isard-admin** if you don't create a multitenancy configuration. E.g. <https://desktops.isardvdi.com/isard-admin>.

![nonpersistent_login](../images/first-steps/login_default.png)

[Administator](../admin/index.md) can create a **multitenant** acces by creating categories. For multitenancy configurations each user should access through it's own category portal in **/isard-admin/login/[category]**.

![nonpersistent_login](../images/first-steps/login_acme.png)
