# Working with Templates

The intention of this guide is to show how to create templates from a desktop, how to manage template actions and how to share the template with other users of the system.

A template is in IsardVDI a basic concept in the desktop creation scheme. A template allows you to set a status on a desktop so it will no longer be modified. Desktops based on this template can be created from then on and that will be identical to the state in which the template was left but from now on these new desktops can be continued to be installed and configured with the desired applicants.

Therefore a template will always be created from a desktop, *freezing* so the state in which the desktop was located and allowing to generate then new new identical desktops to this template.

**ATTENTION**: Deeply review the desktop state before converting it to a template. The template you create will be the desktop in the state in which you leave it and other users will create desktops identical to this template. Therefore DOES NOT LEAVE ANY PERSONAL INFORMATION stored on the desktop from which the template is created as it is also on desktops which are subsequently created from this template. Review the *cache* of the used browsers, the personal data entered in browsers, any downloaded file or key it may have entered. If you accessed the email from this desktop check that your credentials were not stored in your browser. In case of doubt do not turn your desktop into a template or make sure you never share it with other users.

For better understanding of the relationship between desktops and templates see the following scheme. Take your time to understand the relationships explained here as it will be key to understanding the other concepts.

![](./images/derivates.jpg)

Initially an *Ubuntu 16* desktop was created which became a template which is the one we see in the scheme. This template was shared so that other users could create a desktop identical to that of this template.

Other users (example teachers) who had access to this template created their desktops, *Arduino* and *gimp* from the *Ubuntu 16* template. These desktops initially only had the operating system and users (teachers) decided, one installed in arduino software for their students and the other installed in their gimp image editing software. Once they had their desktops with the software they needed they made them templates they shared with other users (with their students in this example).

Finally other users (students in this example) created their respective desktops identical to the *ârduino* and/or *gimp* templates, where they started working with the software already installed.

So we see that there is a direct relationship between the end-desktops created by the users and the templates they derived. There is a tree that is generated as new desktops are created from templates.

Understanding this concept is key to understanding how to work with IsardVDI and why of the outcome of certain actions that we will do with the templates in the following paragraphs.

## Create Template

To create a template we will need to start a desktop that is not booted. Check again that you have not left personal information on your desktop before creating a template.

We will open the desktop details using the blue '+' button and find the *Template it* button there that will open the form to create the template.

![](./images/templateit.png)

In this form it is only required to set a name (which should not be the same as the desktop already has) for the new template to be created.

![](./images/template_form.png)

Do not change the type of template, always use the type *Template*.

Before confirming the creation of the template we could modify its hardware or added media, although it is rare for a template to be needed with these different settings than we were using for the desktop from which it will be created. If it is a desktop we have installed from an ISO it may still be added to the media and we do not want it to happen to the template, so it may be interesting to remove it from the media drop-down.

We will also be able to set the share permissions (*Allows*) with other roles, categories, groups or users at this time if we wish. If we do not change them by default the template that will be created will not be shared with anyone, so we will talk about a private template (it will only be visible to us). Later we can edit the template share again to modify who has access if we wish.

Once you have reviewed the settings we can go on to create the template with the *Create template* button. It is interesting to be clear what this procedure will do to understand how the template system works. Check in the Templates menu (*Templates*) that the template was created.

![](./images/templates.png)

We see that the template *Template Windows 10 pro* has been created and that, since we have not modified share permissions (Allowed) by default, it is only accessible to us and indicates it as a private template.

### How IsardVDI convert the desktop to a template

We explained that the desktop will be frozen in the state in which it was like the new template we created. What has happened is that the desktop disk has become the disk of the new template. That is why we again stress that it is very important to check that no personal data or credentials remain on the desktop from which the template will be created.

Once the disk has been moved to the new template the system will create a new desktop based on this template with the same desktop name as we already had. Everything that is done from now on this desktop is independent of the template, which was already frozen when created.

In this flow diagram we see what happens when we decide to turn the desktop into a template:

![](./images/template_secuence.png)

That is what happens in the steps:

1. Choose a desktop from which to create a new template. We will use the *template it* button that appears when opening the desktop details (sign + blue on the left). We use a name in the template creation form or, as shown in the image, we have accepted the data proposed by the default system.
2. This operation created a template with the given name *Template Windows 10 pro* which we will find in the menu of *Templates* to the left. This template, if we haven't changed the permissions (Allowed) will tell us it's private. This means that for now only we can create new desktops identical to the current state in which the template was created.
2. IsardVDI automatically created our desktop *Windows 10 pro* again. And we say he's created us because the disk he used has moved to the template and to have an identical desktop again and save us from having to create it ourselves, IsardVDI has done it automatically.

Note that this new desktop now depends on the template in that if we remove the template we will be told that this desktop must also be removed (and the other desktops or templates that may have been derived from it).

## Share Template

We can decide whether other users can create a desktop identical to a template by giving them access to that template using the user button on the right, in the share column (*Shares*). This button will open the share form. A shared template will make users who have access to it visible in their add new desktop form.

![](./images/template.png)

In this form we will be able to choose between sharing with all users on our system or only with some of them individually or with the groups, categories or roles of those users.

![](./images/templates_allowed.png)

As seen in the previous image this template is not shared with anyone since no share is enabled. These would be the basic rules for setting shares of a template:

- **Do not share with any system users** (private template): No check-boxes in this form should be checked. The template will only be available because we create desktops derived from it.

![](./images/allowed_none.png)

- **Share with all users of the system**: If we want any user in the system to create a desktop from our template we need to turn on any box (set roles for example, *Set roles*) and leave the associated search field blank (the role field for example, *Roles*).

![](./images/allowed_all.png)

- **Share with some users**: You can set the share with some users individually or with all users in a group, category or with a particular role.
  - With individual users: Select to set users (*Set users*) and type at least two characters in the associated search field (*Users*)
  - With all group users: Select to set groups (*Set groups*) and type at least two characters in the associated search field (*Groups*)
  - With all category users: Select to set categories (*Net categories*) and enter at least two characters in the associated search field (*Categories*)
  - With all users who have roles: Select to set roles (*Set Roles*) and type at least two characters in the associated search field (*Roles*). Currently the predefined system roles are *Administrators*, *Managers*, *Advanced* and *Users*.

Consider the templates too:

- In search fields you can add multiple values to those that will be applied.

- If you enable and select more than one value and type the share will apply to all users. For example in the following example:

![](./images/allowed_mixed.png)

- If you activate a type and leave the associated search field blank (or leave any search field blank and enabled) the template will be shared with all users on the system regardless of what you select in the other types.
- While a template is shared with users they can create a desktop derived from it. Changing the template to private will not disable or remove any existing desktops already created from the template. To remove derived desktops and/or the template the owner of the template must contact the category manager (*manager*) or the system-wide administrator. This is to ensure a check for this operation by a user with permissions. Remember the tree scheme of templates and desktops that would be removed in string as a result.