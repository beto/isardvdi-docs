# Trabajar con plantillas

La intención de esta guía es mostrar como crear plantillas a partir de un escritorio, como gestionar las acciones de una plantilla y como compartir la plantilla con otros usuarios del sistema.

Una plantilla es en IsardVDI un concepto básico en el esquema de creación de escritorios. Una plantilla permite establecer un estado a un escritorio de manera que ya no será modificado de nuevo. A partir de ese momento se podrán crear escritorios basados en esa plantilla y que serán idénticos al estado en el que se dejó la plantilla, pero a partir de ahora esos nuevos escritorios podrán seguir instalando y configurándose con los aplicativos que se desee.

Por lo tanto una plantilla se creará siempre a partir de un escritorio, *congelando* así el estado en el que se encontraba el escritorio y poder generar a partir de esa nueva plantilla nuevos escritorios idénticos.

**ATENCIÓN**: Revise a conciencia el estado del escritorio antes de convertirlo a una plantilla. La plantilla que se cree será el escritorio en el estado en el que la deje y otros usuarios crearan escritorios idénticos a esa plantilla. Por lo tanto <u>NO DEJE NINGUNA INFORMACIÓN PERSONAL</u> almacenada en el escritorio del que se creará la plantilla ya que estará también el los escritorios que posteriormente se creen a partir de esa plantilla. Revise la *cache* de los navegadores usados, los datos personales introducidos en los navegadores, cualquier fichero descargado o clave que pueda haber introducido. Si accedió al correo electrónico desde ese escritorio revise que no quedaron almacenadas en el navegador sus credenciales. En caso de duda no convierta el escritorio en plantilla o bien asegúrese de no compartirla jamás con otros usuarios.

Para que se comprenda mejor la relación entre escritorios y plantillas veamos el siguiente esquema. Tómese su tiempo para entender las relaciones aquí explicadas ya que será clave para entender el resto de conceptos.

![](./images/derivates.jpg)

Inicialmente se creó un escritorio *Ubuntu 16* que se convirtió en una plantilla que es la que vemos en el esquema. Esta plantilla se compartió para que otros usuarios pudieran crear un escritorio idéntico al de esa plantilla.

Otros usuarios (profesores en el ejemplo) que tenían acceso a esa plantilla se crearon sus escritorios, *Arduino* y *gimp* a partir de la plantilla *Ubuntu 16*. Estos escritorios inicialmente solo tenían el sistema operativo y los usuarios (profesores) decidieron, uno instalar dentro programario de arduino para sus alumnos y el otro instaló en el suyo el programario de edición de imágenes gimp. Una vez tuvieron sus escritorios con el programario que necesitaban lo convirtieron en plantillas que compartieron con otros usuarios (con sus alumnos en este ejemplo).

Finalmente otros usuarios (alumnos en este ejemplo) crearon sus respectivos escritorios idénticos a las plantillas de *arduino* y/o de *gimp*, en dónde empezaron a trabajar con el programario ya instalado.

Por lo tanto vemos que hay una relación directa entre los escritorios finales creados por los usuarios y las plantillas de las que derivaron. Existe un árbol que se genera a medida que se van creando nuevos escritorios a partir de plantillas.

Entender este concepto es clave para entender como trabajar con IsardVDI i el porqué del resultado de ciertas acciones que realizaremos con las plantillas en los siguientes apartados.

## Crear una plantilla

Para crear una plantilla deberemos partir de un escritorio que no esté arrancado. Revise una vez más que no ha dejado información personal en el escritorio antes de crear una plantilla.

Abriremos los detalles del escritorio mediante el botón azul '+' y encontraremos allí el botón de *Template it* que nos abrirá el formulario para crear la plantilla.

![](./images/templateit.png)

En este formulario solo es requisito establecer un nombre (que no deberá ser el mismo que ya tiene el escritorio) para la nueva plantilla que se creará.

![](./images/template_form.png)

No cambie el tipo de plantilla, siempre use el tipo *Template*.

Antes de confirmar la creación de la plantilla podríamos modificar su maquinario (*Hardware*) o los medios añadidos (*Media*), aunque es poco habitual que se necesite una plantilla con estos parámetros diferentes de los que estábamos usando para el escritorio del que se creará. Si se trata de un escritorio que hemos instalado desde una ISO es posible que esta aún esté añadida en los medios y no queramos que pase también a la plantilla, con lo que puede ser interesante quitarla ahora del desplegable de medios (*Media*).

También podremos establecer los permisos de compartición (*Allows*) con otros roles, categorías, grupos o usuarios en este momento si lo deseamos. Si no los modificamos por defecto la plantilla que se creará no se compartirá con nadie, con lo cual hablaremos de una plantilla privada (solo será visible para nosotros). Posteriormente podremos editar de nuevo la compartición de la plantilla para modificar quién tiene acceso si lo deseamos.

Una vez revisados los parámetros podremos pasar a crear la plantilla con el botón *Create template*. Es interesante tener claro qué hará este procedimiento para entender como funciona el sistema de plantillas. Comprobemos en el menú de plantillas (*Templates*) que se ha creado la plantilla.

![](./images/templates.png)

Vemos que se ha creado la plantilla *Template Windows 10 pro* y que, como no hemos modificado los permisos de compartición (*Alloweds*) por defecto, solo es accesible por nosotros y nos lo indica como una plantilla privada (*Private*).

### Como IsardVDI convierte el escritorio a una plantilla

Hemos explicado que el escritorio quedará *congelado* en el estado en el que estaba como la nueva plantilla que hemos creado. Lo que ha sucedido es que el disco del escritorio ha pasado a ser el disco de la nueva plantilla. Por eso  recalcamos de nuevo que es importantísimo revisar que no queden datos personales o credenciales en el escritorio del que se creará la plantilla.

Una vez el disco se ha movido a la nueva plantilla el sistema creará un nuevo escritorio basado en esa plantilla con el mismo nombre del escritorio que ya teníamos. Todo lo que se haga a partir de ahora en este escritorio es independiente de la plantilla, que ya quedó congelada al crearse.

En este diagrama de flujo vemos lo que sucede cuando decidimos convertir el escritorio en una plantilla:

![](./images/template_secuence.png)

Esto es lo que sucede en los pasos:

1. Escogemos un escritorio desde el que crearemos una nueva plantilla. Usaremos el botón *template it* que nos aparece al abrir los detalles del escritorio (signo + azul a la izquierda). Rellenamos un nombre en el formulario de creación de plantilla o bien, como se muestra en la imagen, hemos aceptado los datos que nos propone el sistema por defecto.
2. Esta operación ha creado una plantilla con el nombre indicado *Template Windows 10 pro* que encontraremos en el menú de *Templates* a la izquierda. Esta plantilla, si no hemos modificado los permisos (*Alloweds*) nos indicará que es privada. Esto quiere decir que por ahora solo nosotros podremos crear nuevos escritorios idénticos al estado actual en el que se creó la plantilla.
3. IsardVDI automáticamente nos habrá creado de nuevo nuestro escritorio *Windows 10 pro*. Y decimos que nos habrá creado porque el disco que usaba se ha movido a la plantilla y para volver a tener un escritorio idéntico y ahorrarnos tener que crearlo nosotros, IsardVDI lo ha hecho automáticamente.

Cabe tener en cuenta que este nuevo escritorio depende ahora de la plantilla en cuanto a que si eliminamos la plantilla se nos indicará que también debe ser eliminado este escritorio (y el resto de escritorios o plantillas que se hayan podido derivar de esta).

## Compartir una plantilla

Podremos decidir si otros usuarios pueden crear un escritorio idéntico a una plantilla dándoles acceso a esa plantilla mediante el botón de usuarios de la derecha, en la columna de compartición (*Shares*). Este botón abrirá el formulario de compartición. Una plantilla compartida hará que los usuarios que tengan acceso la tengan visible en su formulario de añadir nuevo escritorio.

![](./images/template.png)

En este formulario podremos escoger entre compartir con todos los usuarios de nuestro sistema o bien solo con algunos de ellos, individualmente o bien con los grupos, categorías o roles de esos usuarios.

![](./images/templates_allowed.png)

Como vemos en la imagen anterior esta plantilla no está compartida con nadie ya que no está activada ninguna compartición. Estas serian las reglas básicas para establecer comparticiones de una plantilla:

- **No compartir con ningún usuario del sistema** (plantilla privada): No deberá estar activada ninguna casilla de este formulario. La plantilla solo estará disponible para que nosotros creemos escritorios derivados de ella.

![](./images/allowed_none.png)

- **Compartir con todos los usuarios del sistema**: Si deseamos que cualquier usuario del sistema pueda crear un escritorio a partir de nuestra plantilla deberemos activar una casilla cualquier (la de establecer roles por ejemplo, *Set roles*) y dejar en blanco el campo de búsqueda asociado (el de roles por ejemplo, *Roles*).

![](./images/allowed_all.png)

- **Compartir con algunos usuarios**: Podrá establecer la compartición con algunos usuarios individualmente o bien con todos los usuarios de un grupo, categoría o con un rol determinado.
  - Con usuarios individuales: Seleccione establecer usuarios (*Set users*) y escriba al menos dos caracteres en el campo de búsqueda asociado (*Users*)
  - Con todos los usuarios de grupos: Seleccione establecer grupos (*Set groups*) y escriba al menos dos caracteres en el campo de búsqueda asociado (*Groups*)
  - Con todos los usuarios de categorías: Seleccione establecer categorías (*Set categories*) y escriba al menos dos caracteres en el campo de búsqueda asociado (*Categories*)
  - Con todos los usuarios que tengan los roles: Seleccione establecer roles (*Set roles*) y escriba al menos dos caracteres en el campo de búsqueda asociado (*Roles*). Actualmente los roles del sistema predefinidos son *Administrators*, *Managers*, *Advanced* y *Users*.

Tenga en cuenta respecto a las plantillas también que:

- En los campos de búsqueda podrá añadir múltiples valores a los que se aplicará. 

- Si activa y selecciona más de un valor y tipo la compartición se aplicará con los usuarios que aplique a todas ellas. Por ejemplo en el siguiente ejemplo:

![](./images/allowed_mixed.png)

- Si activa un tipo y deja en blanco el campo de búsqueda asociado (o deja cualquier campo de búsqueda en blanco y activado) la plantilla quedará compartida con todos los usuarios del sistema independientemente de lo que seleccione en el resto de tipos.
- Mientras una plantilla esté compartida con usuarios estos podrán crear un escritorio derivado de esta. El hecho de volver a convertir la plantilla en privada no deshabilitará ni eliminará los escritorios que ya se hayan creado. Para eliminar los escritorios derivados y/o la plantilla el usuario propietario de la plantilla deberá contactar con el gestor de la categoría (*manager*) o bien con el administrador de todo el sistema. Esto es así para asegurar una comprobación de esta operación por parte de un usuario con permisos. Recuerde el esquema en árbol de plantillas y escritorios que serian eliminados en cadena como resultado.



