# Introducció

Els usuaris d'IsardVDI estan classificats en [rols](../manager/users.md#roles). Aquesta secció descriu funcions que poden utilitzar els usuaris amb rol avançat.

Els usuaris amb **rol avançat** tenen les funcionalitats de tots els [usuaris](../user/index.ca.md) a més dels escrits en aquesta secció.
